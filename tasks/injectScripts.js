'use strict';

const inject = require( 'gulp-inject' );

gulp.task( 'injectScripts', () => {

    gulp.src( project.footer.file )
        .pipe( inject( gulp.src( [ paths.srcScript + '/' +  scripts.compiled ], { read : false } ), {
            addRootSlash : typeof addRootSlashOnInject !== 'undefined' ? addRootSlashOnInject : false,
            transform    : function( filepath ) {
                let originalFilepath = inject.transform.apply( inject.transform, arguments );
                return originalFilepath.replace( paths.src + '/', '/' );
            }
        } ) )
        .pipe( gulp.dest( project.footer.path ) )

} );
