'use strict';

const   replace     = require( 'gulp-replace' );

gulp.task( 'copyToDist', () => {

    gulp.src( paths.src + '/app/**/*.*', { base: paths.src } )
        .pipe( gulp.dest( paths.dist ) )
        .on( 'end', () => {
            gulp.src( paths.src + '/core/**/*.*', { base: paths.src } )
                .pipe( gulp.dest( paths.dist ) )
                .on( 'end', () => {
                    gulp.src( paths.src + '/favicons/**/*.*', { base: paths.src } )
                        .pipe( gulp.dest( paths.dist ) )
                        .on( 'end', () => {
                            gulp.src( paths.src + '/vendor/**/*.*', { base: paths.src } )
                                .pipe( gulp.dest( paths.dist ) )
                                .on( 'end', () => {
                                    gulp.src( paths.src + '/index.php', { base: paths.src } )
                                        .pipe( gulp.dest( paths.dist ) )
                                        .on( 'end', () => {
                                            gulp.src( paths.src + '/wp-config.php' , { base: paths.src } )
                                                .pipe( replace( 'define(\'IS_LOCAL\', true)', 'define(\'IS_LOCAL\', false)' ) )
                                                .pipe( gulp.dest( paths.dist ) );
                                        } );
                                } );
                        } );
                } );
        } );

} );
