'use strict';

const inject = require( 'gulp-inject' );

gulp.task( 'injectSass', () => {

    gulp.src( paths.srcStylesheet + '/style.scss' )
        .pipe( inject( gulp.src( 'third-party/*.scss', { cwd: paths.srcStylesheet, read: false } ), {
            starttag  : '/* inject:importThirdParty */',
            endtag    : '/* endinject */',
            transform : function( filepath ) {
                return '@import "..' + filepath + '";';
            }
        } ) )
        .pipe( gulp.dest( paths.srcStylesheet ) )
        .on( 'end', () => {
            gulp.src( paths.srcStylesheet + '/style.scss' )
                .pipe( inject( gulp.src( 'layout/*.scss', { cwd: paths.srcStylesheet, read: false } ), {
                    starttag  : '/* inject:importLayouts */',
                    endtag    : '/* endinject */',
                    transform : function( filepath ) {
                        return '@import "..' + filepath + '";';
                    }
                } ) )
                .pipe( gulp.dest( paths.srcStylesheet ) )
                .on( 'end', () => {
                    gulp.src( paths.srcStylesheet + '/style.scss' )
                        .pipe( inject( gulp.src( 'module/*.scss', { cwd: paths.srcStylesheet, read: false } ), {
                            starttag  : '/* inject:importModules */',
                            endtag    : '/* endinject */',
                            transform : function( filepath ) {
                                return '@import "..' + filepath + '";';
                            }
                        } ) )
                        .pipe( gulp.dest( paths.srcStylesheet ) )
                        .on( 'end', () => {
                            gulp.src( paths.srcStylesheet + '/style.scss' )
                                .pipe( inject( gulp.src( 'state/*.scss', { cwd: paths.srcStylesheet, read: false } ), {
                                    starttag  : '/* inject:importStates */',
                                    endtag    : '/* endinject */',
                                    transform : function( filepath ) {
                                        return '@import "..' + filepath + '";';
                                    }
                                } ) )
                                .pipe( gulp.dest( paths.srcStylesheet ) )
                                .on( 'end', () => {
                                    gulp.src( paths.srcStylesheet + '/style.scss' )
                                        .pipe( inject( gulp.src( 'theme/*.scss', { cwd: paths.srcStylesheet, read: false } ), {
                                            starttag  : '/* inject:importThemes */',
                                            endtag    : '/* endinject */',
                                            transform : function( filepath ) {
                                                return '@import "..' + filepath + '";';
                                            }
                                        } ) )
                                        .pipe( gulp.dest( paths.srcStylesheet ) )
                                        .on( 'end', () => {
                                            gulp.src( paths.srcStylesheet + '/style.scss' )
                                                .pipe( inject( gulp.src( 'overrides/*.scss', { cwd: paths.srcStylesheet, read: false } ), {
                                                    starttag  : '/* inject:importOverrides */',
                                                    endtag    : '/* endinject */',
                                                    transform : function( filepath ) {
                                                        return '@import "..' + filepath + '";';
                                                    }
                                                } ) )
                                                .pipe( gulp.dest( paths.srcStylesheet ) )
                                                .on( 'end', () => {
                                                    runsequence( 'wiredepStyles' );
                                                } );
                                        } );
                                } );
                        } );
                } );

        } );

} );
