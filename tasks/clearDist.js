'use strict';

gulp.task( 'clearDist', () => {

    let promises = [];

    promises.push( del( paths.dist + '/app' ) );
    promises.push( del( paths.dist + '/core' ) );
    promises.push( del( paths.dist + '/favicons' ) );
    promises.push( del( paths.dist + '/vendor' ) );
    promises.push( del( paths.dist + '/humans.txt' ) );
    promises.push( del( paths.dist + '/wp-config.php' ) );

    Promise.all( promises ).then( resolve => {
        if ( resolve ) {
            runsequence( 'minifyScripts' );
        }
    } );

} );
