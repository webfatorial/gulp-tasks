'use strict';

const   replace     = require( 'gulp-replace' );

gulp.task( 'updateMainCssReference', () => {

    gulp.src( project.header.file )
        .pipe( replace( /assets\/css\/(.*).css/gi, 'assets/css/' + pkg.name + '-' + RANDOM_STRING + '.css' ) )
        .pipe( gulp.dest( project.header.path ) )
        .on( 'end', () => {
            runsequence( 'copyAssetsBuild' );
        } );

} );
