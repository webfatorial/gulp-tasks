'use strict';

const wiredep = require( 'wiredep' ).stream;

gulp.task( 'wiredepStyles', () => {

    let supressSrcOnWiredeep = typeof project.supressSrcOnWiredeep !== 'undefined' ? project.supressSrcOnWiredeep : false,
        srcDefaultPath       = 'src';

    if ( supressSrcOnWiredeep ) {
        srcDefaultPath       = '';
    }

    let ignorePath  = (project.type == 'wordpress') ? /^(\/|\.+(?!\/[^\.]))+\.+/ : '',
        cssPath     = (project.type == 'wordpress') ? '<link rel="stylesheet" href="<?php echo bloginfo(\'template_url\') ?>/' + srcDefaultPath + '{{filePath}}">'    : '<link href="/{{filePath}}" rel="stylesheet">',
        jsPath      = (project.type == 'wordpress') ? '<script src="<?php echo bloginfo(\'template_url\') ?>/' + srcDefaultPath + '{{filePath}}"></script>'           : '<script src="/{{filePath}}"></script>';

    if ( project.footer && project.header && ( project.footer.path + project.footer.file != project.header.path + project.header.file ) ) {
        gulp.src( [ project.header.file ] )
            .pipe( wiredep( {
                ignorePath: ignorePath,
                fileTypes: {
                    html: {
                        replace: {
                            css : cssPath,
                            js  : jsPath
                        }
                    }
                }
            } ) )
            .pipe( gulp.dest( project.header.path ) )
            .on( 'end', () => {
                runsequence( 'compileStyles' );
            } );
    } else {
        runsequence( 'compileStyles' );
    }

} );
