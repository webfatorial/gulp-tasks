'use strict';

const   concat      = require( 'gulp-concat' ),
        readlines   = require( 'readlines' ),
        uglify      = require( 'gulp-uglify' );

gulp.task( 'minifyScripts', () => {

    del( paths.distJs + '/' + pkg.name + '-*.js' );

    let lines                   = readlines.readlinesSync( project.footer.file ),
        bowerAssetsStarted      = false,
        bowerAssetsFinished     = false,
        bowerAssetsFiles        = [],
        bowerAssetsToInject     = [],
        hasBowerMarkStartString = function( currentLine ) {
            return currentLine.indexOf( 'bower:js' ) > -1;
        },
        hasBowerMarkEndString   = function( currentLine ) {
            return currentLine.indexOf( 'endbower' ) > -1;
        };

    for ( let i in lines ) {
        let currentLine = lines[ i ];

        if ( hasBowerMarkStartString( currentLine ) ) {
            bowerAssetsStarted = true;
        }

        if ( hasBowerMarkEndString( currentLine ) ) {
            bowerAssetsFinished = true;
        }

        if ( bowerAssetsStarted && ! bowerAssetsFinished ) {
            bowerAssetsFiles.push( currentLine.trim() );
        }
    }

    bowerAssetsFiles.shift();

    for ( let i in bowerAssetsFiles ) {
        let currentPath = bowerAssetsFiles[ i ],
            regExp      = new RegExp( '\/bower_components\/(.*)\.js', 'gi' ),
            bowerPath   = currentPath.match( regExp );

        bowerAssetsToInject.push( paths.srcAssets + bowerPath.toString() );
    }

    gulp.src( bowerAssetsToInject.concat( paths.srcScript + '/' + scripts.compiled ) )
        .pipe( concat( pkg.name + '-' + RANDOM_STRING + '.js', { newLine: ';' } ) )
        .pipe( uglify() )
        .pipe( gulp.dest( paths.distJs ) )
        .on( 'end', () => {
            runsequence( 'updateMainJsReference' );
        } );

} );
