'use strict';

gulp.task( 'copyAssets', () => {

    if ( paths.srcImage ) {
        gulp.src(paths.srcImage + '/**/*.{gif,ico,jpg,png,svg}', { base: paths.srcImage })
            .pipe(gulp.dest(paths.distImg))
            .on('end', () => {
                gulp.src(paths.srcFont + '/**/*.{eot,otf,svg,ttf,woff}', { base: paths.srcFont })
                    .pipe(gulp.dest(paths.distFont))
                    .on('end', () => {
                        runsequence('injectSass');
                    });
            });
    } else {
        runsequence('injectSass');
    }

} );
