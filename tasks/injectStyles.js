'use strict';

const inject = require( 'gulp-inject' );

gulp.task( 'injectStyles', () => {

    if ( project.header ) {
        gulp.src(project.header.file)
            .pipe(inject(gulp.src([paths.srcStylesheet + '/style.css'], { read: false }), {
                addRootSlash: typeof addRootSlashOnInject !== 'undefined' ? addRootSlashOnInject : false,
                transform: function (filepath) {
                    let originalFilepath = inject.transform.apply(inject.transform, arguments);
                    return originalFilepath.replace(paths.src + '/', '/');
                }
            }))
            .pipe(gulp.dest(project.header.path))
            .on('end', () => {
                runsequence('wiredepScripts');
            });
    } else {
        runsequence('wiredepScripts');
    }

} );
