'use strict';

const   autoprefixer    = require( 'gulp-autoprefixer' ),
        compass         = require( 'gulp-compass' ),
        mmq             = require( 'gulp-merge-media-queries' );

global.compileStylesFlag = false;

gulp.task( 'compileStyles', () => {

    gulp.src( paths.srcStylesheet + '/style.scss' )
        .pipe( plumber( {
            handleError: err => {
                console.log( err );
                this.emit( 'end' );
            }
        } ) )
        .pipe( compass( {
            sass        : paths.srcStylesheet,
            css         : paths.distCss,
            image       : paths.distImg,
            javascript  : paths.distJs,
            font        : paths.distFont,
            import_path : 'node_modules',
            relative    : true,
            bundle_exec : false,
            sourcemap   : true,
            time        : true,
            require     : [ 'breakpoint' ]
        } ) )
        .pipe( autoprefixer( { browsers: [ 'last 3 versions' ] } ) )
        .pipe( mmq() )
        .on( 'error', err => {
            console.log( '>>> ERROR', err.message );
        } )
        .pipe( gulp.dest( paths.distCss ) )
        .pipe( livereload() )
        .on( 'end', () => {
            if ( ! compileStylesFlag ) {
                compileStylesFlag = true;
                runsequence( 'injectStyles' );
            }
        } );

} );
